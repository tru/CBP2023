#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

sudo yum -y install environment-modules || :
umask 022
cat <<EOF | sudo tee /etc/modulefiles/ISB2022
#%Module1.0#####################################################################
module-whatis   "enable ISB2022 module path, where the course software is installed"

prepend-path    PATH    /ISB2022/bin
module use /ISB2022/modulefiles
EOF

