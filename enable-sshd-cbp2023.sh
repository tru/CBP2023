#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

echo "enabling password ssh for cbp2023"
sudo grep 'Match User cbp2023' /etc/ssh/sshd_config || \
cat <<EOF| sudo tee -a /etc/ssh/sshd_config
Match User cbp2023
        PasswordAuthentication yes
EOF
sudo restorecon -v /etc/ssh/sshd_config
sudo service sshd restart
