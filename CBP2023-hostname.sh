#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

_x=`/sbin/ip addr sh em2| awk -F' ' '/inet /{print $2}'| sed 's,/.*,,g;s,.*157.99.169.,,g'`
echo CBP2023-${_x}
sudo hostnamectl set-hostname CBP2023-${_x}
