#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

cat <<EOF | sudo tee /var/lib/AccountsService/users/admin
[User]
Language=
XSession=gnome
SystemAccount=true
EOF
