#!/bin/sh
# curl https://gitlab.pasteur.fr/tru/CBP2023/raw/master/c7-sshd_config.sh | sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
export PATH
# sshd/etc/ssh/sshd_config
grep 'Match User cbp2023' /etc/ssh/sshd_config || \
cat <<EOF | sudo tee -a  /etc/ssh/sshd_config
Match User cbp2023
    PasswordAuthentication yes
EOF
#
# fix selinux permissions
/sbin/restorecon -rv /etc/ssh


