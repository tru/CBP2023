#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u
set -o pipefail

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

curl https://dl.pasteur.fr/fop/PUq5O5LC/CBP2023-cups.tgz| sudo tar -C / -xzvf -
sudo restorecon -rv /etc/cups /etc/printcap
sudo service cups restart
lpstat -a
