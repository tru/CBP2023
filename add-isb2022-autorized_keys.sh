#!/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
export PATH
/sbin/useradd -u 1001 -g users -m isb2022
mkdir -p /home/isb2022/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC6XrPB9LLMKR9XGF5IeIeUG/YMLAXU2jYkMirT0BB1EciInEeO3iS4A7xsImaNkj8do9xpEszOPo9/9ksqBAyGtAqe07vB7n98uhwv/rHfKAbhpuZcPTc3JuS3CVmGUEdHyB9KO5Y4Efa48iaereeXIxhpjUkuS7grB4fwOjkq9U8CCN8vg0L/0Ngp4GecGkKK0vSnAkev75Gmhvo2oQGaqvTZHN0vlCvCwGTgXBryetMP3B9UjaGTVzxvbabL/tu1F0LyfHGOPk1SjG75EUm2eCByLDeN8H7hCtXA0SKJYgT+V4It4F/xFbPiD0pQuH0b7VDIzoowvrxWgxduxK1lp5Cdm7uQaZcu84hHvnYxw+g3XEHhZ+SUg+Od/x68Tai25nMrwgVZySa1wnyn/5kBSfu48l+9O1+iX1PlqX31Lct7NhAssc4FAPmoofWpwIzHB6atFIu640ulPgmodl31dGkkZYRD1+JoqH80zKSb1vZ4Y1IP+wmpgxcFeNIlyrF8+EjGet3kz4dTMI4wBO5x9GHwUD11VVnePI77hh9ENFTwckdVyvKihGtidbdp5eOBvcpd2MCoEfX01wAB3ms9Y8Hi+B5tGoeqaDTo1vPj8G6gRHDGYragzvz4oYgjTQdkntv+GaEF/qTQVaS5D0Mtq4IsemaLpPq+4x6sNgo2Yw== students'  > /home/isb2022/.ssh/authorized_keys
chmod 0700 /home/isb2022/.ssh
chmod 0600 /home/isb2022/.ssh/authorized_keys
chown -R isb2022:users /home/isb2022/.ssh
echo pasteur-isb | /usr/bin/passwd --stdin isb2022
restorecon -rv /home/isb2022 /etc
