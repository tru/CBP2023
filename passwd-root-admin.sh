#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

# for i in `cat ~/.dsh/group/salle5`; do echo $i; scp passwd-root-admin.sh $i: && ssh $i ./passwd-root-admin.sh; ssh $i /bin/rm ./passwd-root-admin.sh; done

echo "changing default root and admin password"
echo changing root passwd
echo 'root:$6$O.5Xp6aC$sQyxtDyI7.V0HwHSp30j42cIM4kRzgHL6Hb0gvug3wCViRuK0lS8e7K6amXhh/lr4HedY32mAp/1pmWE3mwIX0' | sudo chpasswd -e 
echo changing admin passwd
echo 'admin:$6$Uy5YNT54$GLGG50GxLA/zSjosSpqmnJDXwIFV7CNUTQy4HLTtZt90rnabRUJI9eTjSajmYbUmrr1cfRcg.oYyGuYiLv6tC0'| sudo chpasswd -e 

