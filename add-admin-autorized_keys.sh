#!/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin
export PATH
/sbin/useradd -u 1000 -g users -G wheel -m admin
mkdir -p /home/admin/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDUZWwgC1TWVAoq2IDYsuIgRWtkGGtVoBjWEXmrsMTcfTXUXBELmV1C7uKvv8OQelJbj/t77dB4lV2pepLxMuKiic5FQ7QDIMBLZ70FR7Jhp8eyy7X5X1SxJnEQmZ0xFdkTJ63Wg9umKD3fkSjhmSoIzDistyTiUEQmXfMzrxiwS8aw08zXEK0nY1PhvtsZm0aHowMi4ZN5oLJcGMIYHZypnxAUMIqcsWfweJ0mjzQdC5eEoWLmjAhIMOvdixSgovqNK+XIg6KdFdmF1TOMoriOt+WN0MXrNWfuVI/XgHsHMjqtRYJz3L1T3bGpd7eGbQPPTbIek4MxEr07t4u5DzYS46btEhobJo93MdhLfrZnT5UUOuXLMJC5q1apI69zKSYylJ9EXkFwR4Zxzh/DUe0usqXbMxpERgCvIa+5jdQtRH6tR6evaKxwZkOspLy5Ti6NlysqLYsZZbfPW23oLwSafEhdbdDqbJyOndxj/B+ApuGqyeL6cUsVoMgFcILY4kovKIfgjeH9lNP01UWYgua9xSrJcV3VMIHJQj/Ds7E+iZsywIdw2QsrQZGfaIHJcND78E1ZF/VmVrUvVHZ19MyusfF2Gbfjd2LPjGKTW1CcXAbe5QkET8Swh9cZHixOR5GR7pr2qbquZ6bsMKBuCIJwlwVbxigai36CJOeXD/pihw== admin' > /home/admin/.ssh/authorized_keys
chmod 0700 /home/admin/.ssh
chmod 0600 /home/admin/.ssh/authorized_keys
chown -R admin:users /home/admin/.ssh
restorecon -rv /home/admin
echo 'admin ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
restorecon -rv /etc/sudoers
echo aio7460-2022 | /usr/bin/passwd --stdin admin
restorecon -rv /home/admin /etc

