#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u
set -o pipefail

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

# adding boot email in /etc/rc.local -> /etc/rc.d/rc.local
# requires adding lines in the file
# AND chmod 755
# Tru 2020/11/27
sudo sed -i -e '/mail -s/d' /etc/rc.d/rc.local
cat <<EOF | sudo tee -a /etc/rc.d/rc.local
(date; dmesg) | mail -s "booting `hostname`" tru@pasteur.fr
EOF

sudo chmod 755 /etc/rc.d/rc.local
