#!/bin/sh
PATH=/sbin:/usr/sbin:/usr/bin:/bin

set -e
set -u

tbindir=$(mktemp -d)
trap "rm -rf \"$tbindir\"" TERM INT EXIT

sudo lvresize -L 150G /dev/mapper/vgdepens-home || :
sudo xfs_growfs /dev/mapper/vgdepens-home || :
df -hP /home
