# COURS DE BIOCHIMIE DES PROTEINES
- 9 janvier 2023 - 10 février 2023
- TP CryoEM sous linux


```
[cbp2023@CBP2023 ~]$ uname -a
Linux CBP2023 3.10.0-1160.81.1.el7.x86_64 #1 SMP Fri Dec 16 17:29:43 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux

[cbp2023@CBP2023 ~]$ lsblk
NAME              MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                 8:0    0 238.5G  0 disk 
├─sda1              8:1    0   200M  0 part /boot/efi
├─sda2              8:2    0  1000M  0 part /boot
└─sda3              8:3    0 237.3G  0 part 
  ├─vgdepens-root 253:0    0  31.3G  0 lvm  /
  ├─vgdepens-swap 253:1    0   9.8G  0 lvm  [SWAP]
  └─vgdepens-home 253:2    0   150G  0 lvm  /home

[cbp2023@CBP2023 ~]$ df -hlP
Filesystem                 Size  Used Avail Use% Mounted on
devtmpfs                   3.8G     0  3.8G   0% /dev
tmpfs                      3.9G     0  3.9G   0% /dev/shm
tmpfs                      3.9G   19M  3.8G   1% /run
tmpfs                      3.9G     0  3.9G   0% /sys/fs/cgroup
/dev/mapper/vgdepens-root   32G   14G   18G  43% /
/dev/mapper/vgdepens-home  150G   77G   74G  51% /home
/dev/sda2                  969M  224M  679M  25% /boot
/dev/sda1                  200M   12M  189M   6% /boot/efi
tmpfs                      779M   56K  779M   1% /run/user/1001

[cbp2023@CBP2023 ~]$ nvidia-smi -L
GPU 0: NVIDIA GeForce GTX 1050 (UUID: GPU-6ade0b7b-04ed-d364-b014-91b61fd943f2)

[cbp2023@CBP2023 ~]$ nvidia-smi 
Wed Jan 25 10:26:16 2023       
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 525.85.05    Driver Version: 525.85.05    CUDA Version: 12.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  NVIDIA GeForce ...  Off  | 00000000:01:00.0 Off |                  N/A |
| N/A   22C    P8    N/A /  N/A |      0MiB /  4096MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+


[admin@CBP2023 ~]$ sudo vgs
  VG       #PV #LV #SN Attr   VSize   VFree  
  vgdepens   1   3   0 wz--n- 237.30g <46.29g
[admin@CBP2023 ~]$ sudo lvs
  LV   VG       Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home vgdepens -wi-ao---- 150.00g                                                    
  root vgdepens -wi-ao----  31.25g                                                    
  swap vgdepens -wi-ao----  <9.77g                                                    

```
